from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from pictures.views import (
    MessageDetailView,
    MessageListView,
    create_message,
    send_email,
)

urlpatterns = [
    path("", MessageListView.as_view(), name="messages-list"),
    path("<int:pk>/", MessageDetailView.as_view(), name="messages-detail"),
    path("<int:pk>/email/", send_email, name="messages-to-email"),
    path("api/message/", create_message, name="api-create-message"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
