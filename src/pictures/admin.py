from django.contrib import admin
from django.contrib.admin import register
from django.contrib.auth.admin import UserAdmin

from pictures.models import Contact, Media, Message, User

admin.site.register(User, UserAdmin)


@register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = [
        "display_name",
        "phone_number",
    ]


class MediaInline(admin.TabularInline):
    model = Media


@register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = [
        "sender",
        "content",
    ]
    inlines = [
        MediaInline,
    ]


@register(Media)
class MediaAdmin(admin.ModelAdmin):
    list_display = [
        "sender",
    ]
