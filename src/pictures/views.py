import base64
import json
from smtplib import SMTPException

from anymail.exceptions import AnymailRequestsAPIError
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.mail import EmailMessage
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from django.views.decorators.http import require_http_methods

from pictures.models import Contact, Media, Message

STATUS_OK = "ok"
STATUS_ERROR = "error"


class MessageListView(generic.ListView):
    model = Message
    template_name = "pictures/messages-list.html"
    context_object_name = "messages"


@method_decorator(ensure_csrf_cookie, name="dispatch")
class MessageDetailView(generic.DetailView):
    model = Message
    template_name = "pictures/messages-detail.html"
    context_object_name = "message"


def send_email(request, pk):
    message = get_object_or_404(Message, pk=pk)
    all_media = message.media_files.all()
    media_count = len(all_media)
    if media_count > 1:
        subject = "Vos photos"
    else:
        subject = "Votre photo"
    message_text = (
        message.content
        + "\n\n-- \nEnvoyé depuis votre cadre photo, par Claire & Gabriel"
    )

    try:
        email = EmailMessage(
            subject,
            message_text,
            from_email=None,
            to=[settings.EMAIL_RECIPIENT],
        )
        for media in all_media:
            email.attach_file(media.file.path)
        email.send(fail_silently=False)
    except (SMTPException, AnymailRequestsAPIError) as e:
        return _error_response(str(e))
    return JsonResponse({"status": STATUS_OK}, status=200)


def _error_response(message: str, status_code: int = 500):
    return JsonResponse(
        {"status": STATUS_ERROR, "message": message}, status=status_code
    )


@csrf_exempt
@require_http_methods(["POST"])
def create_message(request):
    try:
        body = request.body.decode("utf-8")
        data = json.loads(body)
        phone_number = data.get("phone_number")
        if phone_number is None:
            return _error_response("phone_number is required", 400)
        contact, _ = Contact.objects.get_or_create(phone_number=phone_number)
        content = data.get("content", "")
        message = Message.objects.create(sender=contact, content=content)
        base64_data = data.get("image_base64")
        if base64_data:
            mime_type, image_string = base64_data.split(";base64,")
            ext = mime_type.split("/")[-1]
            image_file = ContentFile(
                base64.b64decode(image_string), name=f"{message.pk}.{ext}"
            )
            Media.objects.create(message=message, file=image_file)
    except Exception as e:
        return _error_response(str(e))
    return JsonResponse({"status": STATUS_OK, "data": {"id": message.pk}}, status=201)
