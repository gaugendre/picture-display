import pytest

from pictures.models import Contact


@pytest.fixture
@pytest.mark.django_db
def contact():
    return Contact.objects.create(
        phone_number="+33611111111",
        display_name="Test contact",
    )
