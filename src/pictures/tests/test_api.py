import json

import pytest
from django.test import Client
from django.urls import reverse

from pictures.models import Contact, Message
from pictures.views import STATUS_OK


@pytest.mark.django_db
@pytest.mark.parametrize("content", ["This is a test message", ""])
def test_post_message_existing_contact(client: Client, contact: Contact, content: str):
    assert Message.objects.count() == 0
    res = _post_message(client, str(contact.phone_number), content)
    assert res.status_code == 201
    data = res.json()
    message = Message.objects.first()
    assert data["status"] == STATUS_OK
    assert data["data"]["id"] == message.pk
    assert message.sender == contact
    assert message.content == content


def _post_message(client: Client, phone_number: str, content: str):
    res = client.post(
        reverse("api-create-message"),
        data=json.dumps(
            {
                "phone_number": phone_number,
                "content": content,
            }
        ),
        content_type="application/json",
    )
    return res


@pytest.mark.django_db
@pytest.mark.parametrize("content", ["This is a test message", ""])
def test_post_message_new_contact(client: Client, content: str):
    assert Message.objects.count() == 0
    phone_number = "+33622222222"
    res = _post_message(client, phone_number, content)
    data = res.json()
    assert res.status_code == 201
    message = Message.objects.first()
    assert data["status"] == STATUS_OK
    assert data["data"]["id"] == message.pk
    assert message.sender.phone_number == phone_number
    assert message.sender.display_name == ""
    assert message.content == content
