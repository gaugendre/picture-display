function sendByEmail() {
    const body = new FormData();
    body.set("csrfmiddlewaretoken", getCookie("csrftoken"));
    const bottomLeft = document.querySelector("#bottom-left i");
    bottomLeft.classList = ["fas fa-spinner fa-pulse"];
    fetch(`email/`, {method: 'POST', body: body})
        .then(response => {
            return response.json();
        })
        .then(json => {
            console.log(json);
            if (json.status === "ok") {
                bottomLeft.classList = ["fas fa-check"];
            } else {
                bottomLeft.classList = ["fas fa-exclamation-circle"];
                alert(json.message);
            }
        })
        .catch(reason => {
            bottomLeft.classList = ["fas fa-exclamation-circle"];
            alert(reason);
        });
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
